const path = require("path");
const ejs = require("ejs");
const { mailTransport } = require("./index");
const {
  mailer: {
    auth: { user: from },
  },
} = require("../../config");

module.exports = {
  send: async (mailDetails) => {
    const { to, name, subject = "News Letter", content } = mailDetails;
    const filepath = "./templates/content.ejs";
    const newPath = path.join(__dirname, filepath);
    const template = await ejs.renderFile(newPath, { name, content }, {});
    const data = {
      from,
      to,
      subject,
      html: template,
    };
    return mailTransport.sendMail(data);
  },
};
