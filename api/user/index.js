const express = require("express");
const router = express.Router();
const { uploadHandler } = require("../../middlewares/upload");

const controller = require("./controller");

router.post("/newsletter", uploadHandler(), controller.newsletter);

module.exports = router;
