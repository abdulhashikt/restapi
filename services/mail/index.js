const nodemailer = require('nodemailer');
const {mailer} = require('../../config');

const mailConfig = () => nodemailer.createTransport({...mailer});

module.exports.mailTransport = mailConfig();
