const mongoose = require("mongoose");

const logsSchema = mongoose.Schema(
  {
    newslettername: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

logsSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

module.exports = mongoose.model("Logs", logsSchema);
