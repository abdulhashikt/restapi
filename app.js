const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");
const createError = require("http-errors");
const logger = require("morgan");

const { port } = require("./config");
const db = require("./db");
require('./services/queue/receiver')

const app = express();

app.use(express.urlencoded({ limit: "10mb", extended: false }));
app.use(express.json());


app.use(cors());
app.use(helmet());
app.use(compression());
app.use(logger("dev"));

app.use("/", require("./routes"));

app.use((req, res, next) => next(createError(404)));

app.use(require("./helpers/server-error"));

app.listen(port, () => console.log(`App listening at port ${port}`));
