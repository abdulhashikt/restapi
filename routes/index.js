const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => res.json({ success: true, message: 'Server is Running!!!' }));
router.use('/', require('../api/register'));
router.use('/user', require('../api/user'));

module.exports = router;
