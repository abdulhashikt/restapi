const amqp = require("amqplib/callback_api");
const { QUEUE_NAME, FAIL_QUEUE } = require("../../constants/index");

const Logs = require("../../db/models/logs");
const mailService = require("../../services/mail/mailServices");

amqp.connect("amqp://localhost", (connError, connection) => {
  if (connError) {
    throw connError;
  }
  connection.createChannel((channelError, channel) => {
    if (channelError) {
      throw channelError;
    }
    channel.assertQueue(QUEUE_NAME);
    channel.consume(
      QUEUE_NAME,
      async (payload) => {
        const { newslettername, ...mailDetails } = JSON.parse(payload.content);
        try {
          await mailService.send(mailDetails);
          await Logs.create({ newslettername, email: mailDetails.to });
        } catch (err) {
          channel.assertQueue(FAIL_QUEUE);
          channel.sendToQueue(FAIL_QUEUE, payload.content);
        }
      },
      {
        noAck: true,
      }
    );
  });
});
