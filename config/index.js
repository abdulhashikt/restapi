require("dotenv-flow").config();

module.exports = {
  port: process.env.PORT,
  database: {
    dbUri: process.env.DB_URI,
    poolSize: process.env.POOL_SIZE,
    useNewUrlParser: process.env.USE_NEW_URL_PARSER,
    useCreateIndex: process.env.USE_FIND_AND_MODIFY,
    useUnifiedTopology: process.env.USE_CREATE_INDEX,
    useFindAndModify: process.env.USE_UNIFIED_TOPOLOGY,
  },
  mailer: {
    host: "smtp.gmail.com",
    port: 587,
    secure: false,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
    tls: {
      rejectUnauthorized: false,
    },
  },
};
