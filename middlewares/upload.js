const multer = require("multer");
const { v4: uuid } = require("uuid");
const path = require("path");

const storage = multer.diskStorage({
  destination: "./public/uploads/",
  filename(req, file, cb) {
    cb(null, uuid() + path.extname(file.originalname));
  },
});

module.exports.uploadHandler = ({
  filename = "csv_file",
  type = "single",
  length = "1",
  fields = [],
  ext = [".csv"],
} = {}) => {
  const middleware = multer({
    storage,
    fileFilter(req, file, callback) {
      const currentExt = path.extname(file.originalname);

      if (!ext.includes(currentExt)) {
        console.log("currentExt", currentExt);
        const err = new Error(
          `Forbidden extension ${currentExt}. Supports only ${ext.join(",")}`
        );
        return callback(err);
      }
      return callback(null, true);
    },
    limits: {
      fileSize: 5120 * 5120,
    },
  });
  try {
    if (type === "single") {
      return middleware.single(filename);
    }
    if (type === "array") {
      return middleware.array(filename, parseInt(length, 10));
    }
    if (type === "multiple") {
      return middleware.fields(fields);
    }
  } catch (e) {
    console.log("eeeee", e);
    new Error(e);
  }

  return true;
};
