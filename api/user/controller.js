const path = require("path");
const csv = require("csvtojson");

const User = require("../../db/models/user");

const { publishToQueue } = require("../../services/queue/sender");
const { QUEUE_NAME } = require("../../constants/index");

exports.newsletter = async (req, res, next) => {
  try {
    const filepath = `../../public/uploads/${req.file.filename}`;

    const csvPath = path.join(__dirname, filepath);
    const users = await csv().fromFile(csvPath);

    for (let i = 0; i < users.length; i++) {
      const { email, content, name } = users[i];
      const user = await User.findOne({ email });
      if (user) {
        const { firstname, lastname } = user;
        const mailDetails = {
          to: email,
          name: `${firstname} ${lastname}`,
          content,
          newslettername: name,
        };
        publishToQueue(QUEUE_NAME, JSON.stringify(mailDetails));
      }
    }

    return res.json({
      success: true,
      message: "News Letter API Succssfully send email",
    });
  } catch (e) {
    return next(e);
  }
};
