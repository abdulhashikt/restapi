const amqp = require("amqplib/callback_api");
const { QUEUE_NAME } = require("../../constants/index");

let exportChannel = null;
amqp.connect("amqp://localhost", (connError, connection) => {
  if (connError) {
    throw connError;
  }
  connection.createChannel((channelError, channel) => {
    if (channelError) {
      throw channelError;
    }
    channel.assertQueue(QUEUE_NAME);
    exportChannel = channel;
  });
});

module.exports.publishToQueue = async (queueName, data) => {
  exportChannel.sendToQueue(queueName, new Buffer(data));
};
