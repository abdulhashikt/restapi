const User = require("../../db/models/user");
const { validate } = require("email-validator");

exports.register = async (req, res, next) => {
  try {
    const {
      body: { email, firstname, lastname, age },
    } = req;

    if (!validate(email))
      return res.status(422).json({
        success: false,
        message: "Invalid Email",
      });

    if (!email || !firstname || !lastname || !age)
      return res.status(422).json({
        success: false,
        message: "Missing mandatory fields!",
      });

    const alreadyRegistered = await User.findOne({ email });

    if (alreadyRegistered)
      return res.status(409).json({
        success: false,
        message: "Email already exists!",
      });

    const user = await User.create({
      email,
      firstname,
      lastname,
      age,
    });

    return res.json({
      success: true,
      message: "Registration successful.",
      data: { user },
    });
  } catch (e) {
    return next(e);
  }
};
